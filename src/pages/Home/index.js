import  React, {Component, component}  from "react";
import { StatusBar, View,Text,TextInput,FlatList, TouchableOpacity } from "react-native";
import { Hoshi } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/FontAwesome5'


let database =[
  {judul: 'minum',keterangan: 'nginum'},
  {judul: 'makan',keterangan: "ngakan"},
  {judul: "mandi ",keterangan:"mandih"},
  {judul: 'sholat',keterangan: "apejeng"},
  {judul: "pergi ",keterangan:"ajelen" },
  {judul: "berdiri ",keterangan:"manjeng" },
  {judul: "orang ",keterangan: "oreng"},
  {judul: 'uang ',keterangan: 'pesse'},
  {judul: "belajar",keterangan: "jer ajeren"},
  {judul: 'membaca ',keterangan: 'macah'},
  {judul: 'duduk ',keterangan: 'tojhuk'},
  {judul: 'bangun tidur ',keterangan: 'jegeh tedung'},
  {judul: "sakit ",keterangan: 'sakek'}
  
]

class Home extends Component {
  constructor(props) {
    super (props);
    this.state ={
      text: '',
      data: database
     
    };
  }
  search = () => {
    let data= database

    data = data.filter(item => item.judul.toLocaleLowerCase().includes(this.state.text.toLocaleLowerCase()));

    this.setState({
      data: data
    })
  }
  render(){
    return(
        <View style={{flex:1}}>
          <StatusBar backgroundColor="#0288d1" barStyle="light-content"/>
          
          
          <View style={{marginHorizontal:20, marginVertical:10,backgroundColor:'#F9F7F6'}}>
          <Hoshi
          label={'Masukkan kata kunci'}
          borderColor={'#03a9f4'}
          borderHight={3}
          inputPadding={16}
          backgroundColor={'#F9F7F6'}

          onChangeText={text => this.setState({text: text})}
          value={this.state.text}
          onKeyPress={() => this.search()}

          />
          </View>
            <FlatList
        data={this.state.data}
        renderItem={({item}) =>
        <TouchableOpacity style={{borderWidth :1,borderRadius: 3,marginVertical: 5,marginHorizontal:20,padding: 10,flexDirection:'row',alignItems:'center'}}
          onPress={() => this.props.navigation.navigate('Detail')}>

        <View style={{flex: 1}}>
          <Text style={{fontSize:18, fontWeight:'bold'}} >{item.judul}</Text>
          <Text style={{fontSize:16,marginTop:5}}>{item.keterangan}</Text>
        </View>
          <Icon name="chevron-right" size={25} color="#f0fff0" style={{marginRight: 10}}/>
        </TouchableOpacity>
        }
        keyExtractor={item => item.judul}
      />
          
          
          
          </View>

    );
  }
}
export default Home;