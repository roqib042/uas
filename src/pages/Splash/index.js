import React, {useEffect} from 'react';
import { View, Text,Image,StatusBar} from 'react-native';


var Spinner = require ('react-native-spinkit');
const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.replace('Home')
        }, 5000)
    })
    return (
        <View style={{backgroundColor:'#ADD8E6',flex:1,}}>
                 <StatusBar backgroundColor="#03a9f4" barStyle="light-content"/>

                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontSize:32, fontWeight:'bold', color:'#FFFFFF'}}>KOSA KATA</Text>
                    <Text style={{fontSize: 18, color:'#FFFFFF'}}>indonesia-madura</Text>
                    <Spinner style={{marginTop:20}}size={50} type={'ThreeBounce'} color={'#FFFFFF'}/>
                </View>
                <Text style={{textAlign:'center',fontSize:18, color:'#FFFFFF'}}>moh roqib</Text>
                <Text style={{textAlign:'center',marginBottom:20, color:'#FFFFFF'}}>Version 07 | 2021</Text>
            </View>
    );
};

export default Splash;