import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Splash, Home,Detail } from '../pages';


const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator >
            <Stack.Screen name="Splash" component={Splash}/> 
            <Stack.Screen name="Detail" component={Detail}/>  
            <Stack.Screen name="Home" component={Home}/>  
            
        </Stack.Navigator>
    )
}
export default Route